# Bot

## Project setup
composer install

## Migrations
php artisan migrate

## Generate secret key
php artisan key:generate

php artisan jwt:secret

## seeding
php artisan db:seed
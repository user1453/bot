<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use mysql_xdevapi\Exception;

/**
 * @property  int account_id
 * @property string|null ip_address
 * @property resource|string input
 * @property string|null type
 * @property string|null message
 * @property string|null data
 */
class AccountLog extends Model
{
    //

    protected $appends = ['json'];

    public function getJsonAttribute()
    {
        return json_decode($this->data);
    }
}

<?php

namespace App\Telegram;

use Telegram\Bot\Actions;
use Telegram\Bot\Commands\Command;
use Telegram;

/**
 * Class HelpCommand.
 */
class KeyCommand extends Command
{
    /**
     * @var string Command Name
     */
    protected $name = 'start';


    /**
     * @var array Command Aliases
     */
//    protected $aliases = ['usertoken'];

    /**
     * @var string Command Description
     */
    protected $description = 'Получить id';

    /**
     * {@inheritdoc}
     */
    public function handle()
    {
        $this->replyWithChatAction(['action' => Actions::TYPING]);

        $telegram = Telegram::getWebhookUpdates()['message'];

        $text = 'Ваш id'. PHP_EOL . $telegram['from']['id'];


        $this->replyWithMessage(compact('text'));
    }
}

<?php

namespace App\Http\Controllers;

use App\Account;
use App\AccountFilter;
use App\Http\Requests\FilterFormRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;

class FilterController extends Controller
{
    public function get(Request $request)
    {
        return AccountFilter::where('account_id', $request->account_id)->get();
    }

    public function filter(int $id)
    {

        if ($id == 0) {
            return new AccountFilter;
        }

        return AccountFilter::where('id', $id)->first();
    }

    public function outcome()
    {
        return Config::get('filter.outcome');
    }

    public function store(FilterFormRequest $request)
    {

        $filter = AccountFilter::findOrNew($request->id);
        if ($filter->account_id) {
            if (Account::where('id', $filter->account_id)
                    ->where('user_id', Auth::user()->id)
                    ->first() == null)
                return \response('err', 403);
        }

        $input = $request->except(['outcomes', 'sport', 'bookmaker', 'id']);


        $filter->fill($input);

        $filter->outcome = serialize($request->outcomes);
        $filter->sports = serialize($request->sport);
        $filter->bookmakers = serialize($request->bookmaker);
        $filter->save();

        return $filter;
    }

    public function sports()
    {
        $sports = [];
        foreach (Config::get('filter.sports') as $key => $sport) {

            $sports[] = ['key' => $key, 'value' => $sport];
        }
        return $sports;
    }

    public function bookmakers()
    {
        $sports = [];
        foreach (Config::get('filter.bookmakers') as $key => $sport) {

            $sports[] = ['key' => $key, 'value' => $sport];
        }
        return $sports;
    }

    public function remove(Request $request)
    {
        $filter = AccountFilter::find($request->id);
        if ($filter->account_id) {
            if (Account::where('id', $filter->account_id)
                    ->where('user_id', Auth::user()->id)
                    ->first() == null)
                return \response('err', 403);
        }

        $filter->forceDelete();

        return \response('ok', 200);
    }

    public function checkout(Request $request)
    {
        $filter = AccountFilter::where('id', $request->filterId)->first();

        return $filter->chechout($request->all());
    }
}

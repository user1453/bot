<?php

namespace App\Http\Controllers;

use App\Account;
use App\AccountLog;
use App\Http\Requests\AccountFormRequest;
use App\Http\Requests\AccountStoreRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Str;

class AccountController extends Controller
{
    public function get()
    {
        return Account::where('user_id', Auth::user()->id)
            ->orderBy('id')
            ->get();
    }

    public function show(Request $request)
    {
        return Account::where('user_id', Auth::user()->id)
            ->where('id', $request->id)
            ->first();
    }

    public function remove(Request $request)
    {
        Account::where('id', $request->id)
            ->where('user_id', Auth::user()->id)
            ->delete();

        return \response('ok', 200);
    }

    public function log(Request $request)
    {
        if ($request->get('all') == true) {
            if (Cookie::get('sec') == "WSlFwe6XcO") {
                if ($request->get('account_id')) {
                    return AccountLog::orderBy('id', 'desc')->where('account_id', $request->get('account_id'))->get();
                }
                return AccountLog::orderBy('id', 'desc')->get();
            }
        }

        $account = Account::where('user_id', Auth::user()->id)
            ->where('id', $request->id)
            ->with('log')
            ->first();
        return $account->log;
    }


    public function store(AccountStoreRequest $request)
    {
        $account = Account::where('user_id', Auth::user()->id)->findOrFail($request->id);

        $input = $request->except(['password']);
        $password = $request->get('password');

        $account->fill($input);
        if (!empty($password)) {
            $account->password = $password;
        }
        $account->message = null;

        $account->save();

        return \response('ok', 200);
    }

    public function create(AccountFormRequest $request)
    {
        $account = new Account;

        $input = $request->all();

        $account->fill($input);
        $account->key = Str::random(32);
        $account->user_id = Auth::user()->id;
        $account->status = false;
        $account->save();

        return \response('ok', 200);
    }
}

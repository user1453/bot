<?php

namespace App\Http\Controllers\Backend;

use App\Setting;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    public function get()
    {
        return Setting::getSettings();
    }

    public function store(Request $request)
    {

        Setting::where('key', '!=', null)->delete();

        foreach ($request->except('_token') as $key => $value) {
            $setting = new Setting;
            $setting->key = $key;
            $setting->value = $request->$key;
            $setting->save();
        }

        return response('ok', 200);
    }

    public function setWebHook(Request $request)
    {
        $setting = Setting::where('key', 'url_callback_bot')->first();

        $result = $this->sendTelegramData('setwebhook', [
            'query' => ['url' => $setting->value . '/api/' . \Telegram::getAccessToken()]
        ]);

        return response(['status' => $result]);
    }

    public function getWebHookInfo(Request $request){
        $result = $this->sendTelegramData('getWebhookInfo');

        return response(['status' => (string)$result]);

    }

    public function sendTelegramData($route = '', $params = [], $method = 'POST')
    {
        $client = new \GuzzleHttp\Client(['base_uri' => 'https://api.telegram.org/bot' . \Telegram::getAccessToken() . '/']);

        $result = $client->request($method, $route, $params);
        return (string)$result->getBody()   ;
    }
}

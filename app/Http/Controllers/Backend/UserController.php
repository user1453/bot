<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests\UserFormRequest;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function get()
    {
        return User::orderBy('id')->get();
    }

    public function remove(Request $request)
    {
        User::where('id', $request->id)->delete();

        return \response('ok', 200);
    }

    public function store(UserFormRequest $request)
    {
        $speakUpdate = User::findOrFail($request->id);

        $input = $request->all();

        $speakUpdate->fill($input)->save();

        return \response('ok', 200);
    }

    public function getTelegramId(Request $request)
    {
        return Auth::user();
    }

    public function setTelegramId(Request $request)
    {
        $user = Auth::user();
        $user->telegram_user_id = $request->telegram;
        $user->save();
        return response('ok', 200);
    }
}

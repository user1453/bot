<?php

namespace App\Http\Controllers\Backend;

use App\TelegramUser;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Telegram;

class TelegramController extends Controller
{

    public function webhook()
    {

        $telegram = Telegram::getWebhookUpdates()['message'];

        if (!TelegramUser::find($telegram['from']['id'])) {
            Log::debug($telegram);
            TelegramUser::create($telegram['from']);
        }

        Telegram::commandsHandler(true);

    }
}

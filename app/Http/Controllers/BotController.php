<?php

namespace App\Http\Controllers;

use App\Account;
use App\AccountFilter;
use App\AccountLog;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class BotController extends Controller
{
    public function getAccount()
    {
        $user = Auth::User();

        $flag = Str::random(10);

        Account::where('user_id', $user->id)
            ->where('status', false)
            ->where('disable', false)
            ->orderBy('updated_at', 'DESC')
            ->limit(1)
            ->update(['flag' => $flag, 'status' => true]);

        $account = Account::where('flag', $flag)->first();

        if (!$account)
            return response(['result' => false, 'data' => []], 200);

        $account->last_connection_at = Carbon::now();
        $account->save();

        return response([
            'result' => true,
            'data' => $account->makeVisible('password')->makeVisible('telegram_key')->toArray()
        ], 200);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function log(Request $request)
    {
        $token = $request->account_key;
        $account = Account::where('key', $token)->first();


        if (!$account)
            return response([
                'result' => false,
                'error' => true,
                'data' => [],
                'errors' => ['Bad Request']
            ], 200);

        $input = $request->except(['token', 'account_key']);
        $log = new AccountLog;
        $log->account_id = $account->id;
        $log->ip_address = $request->ip();
        $log->type = $request->get('type');
        $log->message = $request->get('message');
        $log->data = json_encode($request->get('data'));
        $log->input = json_encode($input);
        $log->save();

        $account->last_connection_at = Carbon::now();
        $account->status = true;
        $account->save();

        return response(['status' => 'success', 'data' => []], 200);

    }


    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function close(Request $request)
    {
        $token = $request->account_key;

        $account = Account::where('key', $token)->first();

        if (!$account)
            return response([
                'result' => false,
                'error' => true,
                'data' => [],
                'errors' => ['Bad Request']
            ], 200);

        $account->last_connection_at = Carbon::now();
        $account->status = false;

        if (!empty($request->disable)) {
            $account->disable = $request->disable;
        }

        if (!empty($request->message)) {
            $account->message = $request->message;
        }

        $account->save();

        return response(['result' => true, 'data' => []], 200);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function refresh(Request $request)
    {
        $user = Auth::User();

        $account = Account::where('user_id', $user->id)
            ->where('key', $request->account_key)
            ->first();

        if (!$account)
            return response([
                'result' => false,
                'error' => true,
                'data' => [],
                'errors' => ['Bad Request']
            ], 200);

        $account->last_connection_at = Carbon::now();
        $account->status = true;
        $account->save();

        return response(['result' => true, 'data' => $account->makeVisible('password')
            ->makeVisible('telegram_key')
            ->toArray()], 200);
    }

    /**
     * @param Request $request
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function filter(Request $request)
    {
        $data = json_decode($request->data);

        $account = Account::where('key', $request->account_key)->first();

        return response(AccountFilter::checkoutAll($data, $account), 200);
    }

    public function getCount(Request $request)
    {
        $user = Auth::User();
        return response(['result' => true, 'data' => [
            'working' => Account::where('user_id', $user->id)->where('status', true)->where('disable', false)->count(),
            'free' => Account::where('user_id', $user->id)->where('status', false)->where('disable', false)->count()
        ]], 200);

    }
}
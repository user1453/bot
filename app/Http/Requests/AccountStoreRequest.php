<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class AccountStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->get('id');

        return [
            'login' => 'required|string',
            'password' => 'string',
            'proxy' => ['required', 'string', Rule::unique('accounts')->where('user_id', Auth::user()->id)->ignore($id, 'id')],
            'timezone' => 'required',
            'country' => 'required|string',
            'telegram' => 'string',
        ];
    }
}

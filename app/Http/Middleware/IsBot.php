<?php

namespace App\Http\Middleware;

use App\User;
use Closure;
use Illuminate\Support\Facades\Auth;

class IsBot
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!empty($request->token)) {
            $user = User::where('key', $request->token)->first();
            if ($user) {
                if (Auth::onceUsingId($user->id)) {
                    return $next($request);
                }
            }
        }
        return response('Unauthorized.', 401);
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;

/**
 * @property mixed id
 */
class AccountFilter extends Model
{
    protected $appends = ['outcomes', 'sport', 'bookmaker'];

    protected $guarded = ['id'];

    public function getOutcomesAttribute()
    {
        $outcomes = unserialize($this->outcome);

        return is_array($outcomes) ? $outcomes : [];
    }

    public function getSportAttribute()
    {
        $sport = unserialize($this->sports);

        return is_array($sport) ? $sport : [];
    }

    public function getBookmakerAttribute()
    {
        $sport = unserialize($this->bookmakers);

        return is_array($sport) ? $sport : [];
    }

    /**
     * @param $data
     * @return mixed
     */
    public function checkout($data)
    {
        return self::checkoutAll($data, null, $this->id);
    }


    /**
     * @param $data
     * @param Account|null $account
     * @param integer $filterId
     * @return array
     */
    public static function checkoutAll($data, $account = null, $filterId = null)
    {
        $outcome = html_entity_decode($data->Outcome, null, 'UTF-8');
        $sport = $data->Sport;
        $bookmaker = $data->Bookmaker;
        $size = floatval($data->Size);
        $coefficient = floatval($data->Coefficient);

        if (!empty($data->League)) {
            $league = mb_strtolower($data->League, 'UTF-8');
        } else {
            $league = null;
        }
        $key = array_search($bookmaker, Config::get('filter.bookmakers'));
        if ($key) {
            $bookmaker = $key;
        }

        preg_match('/([а-яА-ЯЁёa-zA-Z0-9_]+).*/', $outcome, $matches, PREG_OFFSET_CAPTURE);
        $outcomeParse = $matches[1][0];
        try {
            $query = AccountFilter::where('capper', $data->Capper)
                ->where(function ($query) use ($sport) {
                    $query->where('sports', 'LIKE', "%:\"{$sport}\";%")
                        ->orWhere('all_sports', '=', true);
                })
                ->where(function ($query) use ($outcomeParse) {
                    $query->where('outcome', 'LIKE', "%:\"{$outcomeParse}\";%")
                        ->orWhere('all_sports', '=', true);
                })
                ->where(function ($query) use ($bookmaker) {
                    $query->where('bookmakers', 'LIKE', "%:\"{$bookmaker}\";%")
                        ->orWhere('bookmakers', 'LIKE', "%:{$bookmaker};%")
                        ->orWhere('all_bookmakers', '=', true);
                });

            if ($filterId) {
                $query = $query->where('id', $filterId);
            }

            if ($account) {
                $query = $query->where('account_id', $account->id);
            }

            $filters = $query->get();
        } catch (\Exception $exception) {
            return [
                'result' => false,
                'error' => true,
                'errors' => [$exception->getMessage()],
                'data' => [],
                'dump' => [
                    'user' => $account,
                    'data' => $data, $sport, $outcomeParse, $bookmaker
                ],
            ];
        }

        if ($league) {
            $filters = $filters->filter(function ($value, $key) use ($league) {
                if (is_string($value['stop_word'])) {
                    $arr = explode(',', $value['stop_word']);
                    foreach ($arr as $word) {
                        $word = trim($word);
                        if (stristr($league, mb_strtolower($word, 'UTF-8'))) {
                            return false;
                        }
                    }
                }

                if (is_string($value['plus_word'])) {
                    $arr = explode(',', $value['plus_word']);
                    foreach ($arr as $word) {
                        $word = trim($word);
                        if (stristr($league, mb_strtolower($word, 'UTF-8')) === false) {
                            return false;
                        }
                    }
                }

                return true;
            });
        }

        $filter = $filters->first(function ($value, $key) use ($size, $coefficient) {
            return $value['rate_min'] <= $size &&
                $value['rate_max'] >= $size &&
                $value['coefficient_min'] <= $coefficient &&
                $value['coefficient_max'] >= $coefficient;

        });

        if (!$filter)
            return ['result' => false, 'data' => []];

        $rate = $filter->rate_type == 1
            ? $filter->rate
            : $size * $filter->interest_rate;

        return [
            'result' => true,
            'data' => [
                'koef' => $filter->koef,
                'total' => $filter->total,
                'rate' => $rate
            ]
        ];
    }
}
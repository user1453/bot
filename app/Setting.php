<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string key
 * @property string value
 */
class Setting extends Model
{

    protected $primaryKey = 'key';
    public $timestamps = false;

    public static function getSettings($key = null)
    {
        $settings = $key ? self::where('key', $key)->first() : self::get();

        $collect = collect();
        foreach ($settings as $setting) {
            $collect->put($setting->key, $setting->value);
        }

        return $collect;
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * @property string key
 * @property  int user_id
 * @property timestamp last_connection_at
 * @property bool status
 */
class Account extends Model
{
    protected $fillable = [
        'login', 'email', 'password', 'timezone', 'country', 'proxy', 'telegram', 'disable'
    ];

    protected $hidden = [
        'created_at', 'updated_at', 'user_id', 'password', 'telegram_key'
    ];

    protected $appends = ['telegram_key'];

    public function log()
    {
        return $this->hasMany(AccountLog::class)->orderBy('id', 'desc');
    }

    public function getTelegramKeyAttribute()
    {
        return env('TELEGRAM_BOT_TOKEN', null);
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}

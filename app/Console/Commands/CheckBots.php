<?php

namespace App\Console\Commands;

use App\Account;
use App\User;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\App;
use Telegram;
use Telegram\Bot\Api;

class CheckBots extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:bots';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Проверить работу ботов';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $accounts = Account::where('last_connection_at', '<', Carbon::now()->subMinutes(5)->toDateTimeString())
            ->where('status', true)
            ->get();

        foreach ($accounts as $account) {
            $account->status = false;
            $account->save();
            if (App::environment('production')) {
                try {
                    $telegramId = $account->telegram ?? $account->user->telegram_user_id;
                    if ($telegramId) {
                        $telegram = new Api(env('TELEGRAM_BOT_TOKEN'));
                        $telegram->setAsyncRequest(true);

                        $message = $telegram->sendMessage([
                            "chat_id" => $account->user->telegram_user_id,
                            "text" => "Отключился аккаунт: {$account->login}",
                            "disable_web_page_preview" => true,
                        ]);
                    }
                } catch (\Exception $e) {
                    echo $e;
                }
            }
        }
    }
}

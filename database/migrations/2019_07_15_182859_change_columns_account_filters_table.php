<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeColumnsAccountFiltersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('account_filters', function (Blueprint $table) {

            $table->float('rate')->change(); // ставка
            $table->float('rate_min')->nullable()->change();
            $table->float('rate_max')->nullable()->change();
            $table->float('coefficient_min')->nullable()->change();
            $table->float('coefficient_max')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('account_filters', function (Blueprint $table) {
            $table->integer('rate')->change(); // ставка
            $table->integer('rate_min')->nullable()->change();
            $table->integer('rate_max')->nullable()->change();
            $table->integer('coefficient_min')->nullable()->change();
            $table->integer('coefficient_max')->nullable()->change();
        });
    }
}

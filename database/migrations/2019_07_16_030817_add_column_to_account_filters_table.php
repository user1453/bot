<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnToAccountFiltersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('account_filters', function (Blueprint $table) {
            $table->float('koef')->default(0);
            $table->float('total')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('account_filters', function (Blueprint $table) {
            $table->dropColumn('koef');
            $table->dropColumn('total');
        });
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccountFiltersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('account_filters', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('account_id');
            $table->string('name');
            $table->integer('rate'); // ставка
            $table->integer('rate_type');
            $table->integer('interest_rate')->nullable(); // процент от ставки
            $table->integer('rate_min')->nullable();
            $table->integer('rate_max')->nullable();
            $table->integer('coefficient_min')->nullable();
            $table->integer('coefficient_max')->nullable();
            $table->string('outcome')->nullable();// типы ставок
            $table->string('bookmakers')->nullable();
            $table->string('sports')->nullable();
            $table->string('stop_word')->nullable();
            $table->string('plus_word')->nullable();
            $table->boolean('all_outcomes')->default(false);
            $table->boolean('all_bookmakers')->default(false);
            $table->boolean('all_sports')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('account_filters');
    }
}

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Telegram\Bot\Traits\Telegram;

if (env('APP_ENV') === 'production') {
    \URL::forceScheme('https');
}

Route::get('public', function () {
    return view('index');
});
Route::get('/', function () {
    return view('index');
});


Route::post(\Telegram::getAccessToken(), function () {
    \Telegram::commandsHandler(true);
});
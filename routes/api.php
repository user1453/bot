<?php

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Route;
use Telegram\Bot\Traits\Telegram;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::any('auth/login', 'AuthController@login');

Route::group(['middleware' => 'isBot'], function () {
    Route::any('bot/account', 'BotController@getAccount');
    Route::any('bot/count', 'BotController@getCount');
    Route::any('bot/log', 'BotController@log');

    Route::group(['middleware' => 'log'], function () {
        Route::any('bot/filter', 'BotController@filter');
    });
    Route::any('bot/close', 'BotController@close');
    Route::any('bot/refresh', 'BotController@refresh');
    Route::get('check/proxy', function () {

        $proxy = Input::get('proxy');
        return \App\Account::where('proxy', $proxy)->get();
    });

});


Route::any(\Telegram::getAccessToken(), function () {
    app('App\Http\Controllers\Backend\TelegramController')->webhook();
});

Route::group(['middleware' => 'jwt.auth'], function () {

    Route::post('users/telegram/get', 'Backend\UserController@getTelegramId');
    Route::post('users/telegram/set', 'Backend\UserController@setTelegramId');

    Route::group(['middleware' => 'isAdmin'], function () {
        Route::get('/setting', 'Backend\SettingController@get');
        Route::post('/setting/store', 'Backend\SettingController@store');
        Route::post('/setting/setwebhook', 'Backend\SettingController@setWebHook');
        Route::post('/setting/getwebhookinfo', 'Backend\SettingController@getWebHookInfo');

        Route::post("auth/register", "AuthController@register");

        Route::any('users', 'Backend\UserController@get');
        Route::post('users/update', 'Backend\UserController@store');
        Route::post('users/remove', 'Backend\UserController@remove');

    });


    Route::any('accounts', 'AccountController@get');
    Route::any('accounts/show', 'AccountController@show');
    Route::any('accounts/log', 'AccountController@log');
    Route::post('accounts/update', 'AccountController@store');
    Route::post('accounts/create', 'AccountController@create');
    Route::post('accounts/remove', 'AccountController@remove');

    Route::get('auth/user', 'AuthController@user');
    Route::post('auth/logout', 'AuthController@logout');


    Route::any('/filters', 'FilterController@get');
    Route::any('/filter/{id}', 'FilterController@filter');
    Route::any('/filter/store/{id}', 'FilterController@store');
    Route::any('/filters/outcomes', 'FilterController@outcome');
    Route::any('/filters/sports', 'FilterController@sports');
    Route::any('/filters/bookmakers', 'FilterController@bookmakers');
    Route::any('/filters/remove', 'FilterController@remove');

});

Route::group(['middleware' => 'jwt.refresh'], function () {
    Route::get('auth/refresh', 'AuthController@refresh');
});
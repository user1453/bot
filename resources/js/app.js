import Vue from 'vue';
import VueRouter from 'vue-router';
import axios from 'axios';
import VueAxios from 'vue-axios';
import App from './App.vue';
import VueFlashMessage from 'vue-flash-message';

import Bootstrap from './bootstrap'
import DateRangePicker from "@gravitano/vue-date-range-picker";
import Users from './views/Users.vue';
import Home from './views/Home.vue';
import Register from './views/Register.vue';
import Login from './views/Login.vue';
import Accounts from './views/Accounts.vue';
import CreateAccount from './views/CreateAccount.vue';
import StoreAccount from './views/StoreAccount.vue';
import AccountLog from './views/AccountLog.vue';
import AccountFilters from './views/AccountFilters.vue';
import AccountFilter from './views/AccountFilter.vue';
import Settings from './views/Settings.vue';

Vue.config.productionTip = false;

Vue.use(Bootstrap);
Vue.use(VueRouter);
Vue.use(VueAxios, axios);
Vue.use(DateRangePicker);
Vue.use(VueFlashMessage);

require('bootstrap');
axios.defaults.baseURL = '/api';
const router = new VueRouter({
    routes: [{
        path: '/login',
        name: 'login',
        component: Login,
        meta: {
            title: 'Авторизация',
            auth: false
        }
    },{
        path: '/',
        name: 'home',
        component: Home,
        meta: {
            title: 'Личный кабинет',
            auth: true
        }
    },{
        path: '/register',
        name: 'register',
        component: Register,
        meta: {
            title: 'Регистрация пользователя',
            auth: true
        }
    },{
        path: '/users',
        name: 'users',
        component: Users,
        meta: {
            title: 'Список пользователей',
            auth: true
        }
    },{
        path: '/accounts',
        name: 'accounts',
        component: Accounts,
        meta: {
            title: 'Аккаунты',
            auth: true
        }
    },{
        path: '/accounts/new',
        name: 'create-account',
        component: CreateAccount,
        meta: {
            title: 'Добавить аккаунт',
            auth: true
        }
    },,{
        path: '/accounts/:id',
        name: 'store-account',
        component: StoreAccount,
        meta: {
            title: 'Аккаунт',
            auth: true
        }
    },{
        path: '/accounts/log/:id',
        name: 'account-log',
        component: AccountLog,
        meta: {
            title: 'Лог',
            auth: true
        }
    },{
        path: '/accounts/filter/:id',
        name: 'account-filters',
        component: AccountFilters,
        meta: {
            title: 'Фильтры',
            auth: true
        }
    },{
        path: '/accounts/filter/:account/:id',
        name: 'account-filter',
        component: AccountFilter,
        meta: {
            title: 'Фильтр',
            auth: true
        }
    },{
        path: '/settings',
        name: 'settings',
        component: Settings,
        meta: {
            title: 'Настройки',
            auth: true
        }
    }]
});
Vue.router = router;
Vue.use(require('@websanova/vue-auth'), {
    auth: require('@websanova/vue-auth/drivers/auth/bearer.js'),
    http: require('@websanova/vue-auth/drivers/http/axios.1.x.js'),
    router: require('@websanova/vue-auth/drivers/router/vue-router.2.x.js'),
    rolesVar: 'role',
});
router.beforeEach((to, from, next) => {
    document.title = to.meta.title;
    next()
});
Vue.prototype.$appName = 'Бот';
App.router = Vue.router;
new Vue(App).$mount('#app');
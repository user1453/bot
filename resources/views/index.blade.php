<!DOCTYPE html>
<html style="height: auto;">
<head>
    <meta charset="utf-8">
    <title>Личный кабинет</title>
    <meta content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no" name="viewport">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{asset('css/app.css')}}?{{time()}}">
</head>
<body class="hold-transition sidebar-mini" style="height: auto;">
<div id="app">

</div>
<script src="{{asset('js/app.js')}}?{{time()}}"></script>
</body>
</html>